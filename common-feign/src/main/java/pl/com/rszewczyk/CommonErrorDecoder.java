package pl.com.rszewczyk;

import feign.Response;
import feign.codec.ErrorDecoder;
import jakarta.annotation.Nullable;

public class CommonErrorDecoder implements ErrorDecoder {

  @Nullable
  @Override
  public Exception decode(String s, Response response) {
    return null;
  }
}
