package pl.com.rszewczyk.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import pl.com.rszewczyk.FeignConfiguration;

@Configuration
@Import(FeignConfiguration.class)
public class FeignAutoConfig {}
