package pl.com.rszewczyk;

import feign.Contract;
import feign.Logger;
import javax.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.openfeign.FeignFormatterRegistrar;
import org.springframework.context.annotation.Bean;
import org.springframework.format.datetime.standard.DateTimeFormatterRegistrar;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class FeignConfiguration {

  @Bean
  public Logger.Level feignLoggerLevel(
      @NotNull @Value("${rszewczyk.feign.logging.level:BASIC}") String level) {
    return Logger.Level.valueOf(level);
  }

  @Bean
  public Contract contract() {
    return new Contract.Default();
  }

  @Bean
  public FeignFormatterRegistrar localDateFeignFormatterRegistrar() {
    return formatterRegistry -> {
      DateTimeFormatterRegistrar registrar = new DateTimeFormatterRegistrar();
      registrar.setUseIsoFormat(true);
      registrar.registerFormatters(formatterRegistry);
    };
  }
}
