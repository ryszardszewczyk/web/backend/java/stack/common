package pl.com.rszewczyk

import feign.Client
import feign.Contract
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class FeignTestConfiguration {

  @Value('${local.server.port}')
  private int port

  @Bean
  Contract contract() {
    return new Contract.Default()
  }

  @Bean
  Client client() {
    return new Client.Proxied(
        null, null, new Proxy(Proxy.Type.HTTP, new InetSocketAddress('localhost', port)))
  }
}
