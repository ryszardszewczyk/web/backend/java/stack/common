package pl.com.rszewczyk.common.versionedentity;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import jakarta.persistence.AttributeOverride;
import jakarta.persistence.AttributeOverrides;
import jakarta.persistence.Column;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinColumns;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import jakarta.persistence.Version;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import java.time.OffsetDateTime;
import java.util.UUID;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.javers.core.metamodel.annotation.DiffIgnore;
import pl.com.rszewczyk.common.usersnap.entity.UserId;
import pl.com.rszewczyk.common.usersnap.entity.UserSnap;

@Data
@SuperBuilder
@MappedSuperclass
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@SuppressWarnings({"PMD.AbstractClassWithoutAbstractMethod", "AbstractClassNeverImplemented"})
@SuppressFBWarnings({"EI_EXPOSE_REP", "USBR_UNNECESSARY_STORE_BEFORE_RETURN"})
public abstract class VersionedEntity {

  @Id
  @GeneratedValue
  @EqualsAndHashCode.Include
  @Column(columnDefinition = "uuid", updatable = false)
  private UUID id;

  @NotNull private OffsetDateTime created;

  private OffsetDateTime modified;

  @Version
  @NotNull
  @Min(0)
  private Integer version;

  @Setter(AccessLevel.PRIVATE)
  @NotNull
  @AttributeOverrides({
    @AttributeOverride(name = "id", column = @Column(name = "author_id", updatable = false)),
    @AttributeOverride(name = "type", column = @Column(name = "author_type", updatable = false))
  })
  @Valid
  private UserId authorId;

  @DiffIgnore
  @ToString.Exclude
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumns({
    @JoinColumn(
        name = "author_id",
        columnDefinition = "uuid",
        insertable = false,
        updatable = false,
        referencedColumnName = "id"),
    @JoinColumn(
        name = "author_type",
        insertable = false,
        updatable = false,
        referencedColumnName = "type")
  })
  @Valid
  private UserSnap author;

  @Setter(AccessLevel.PRIVATE)
  @AttributeOverrides({
    @AttributeOverride(name = "id", column = @Column(name = "modifier_id")),
    @AttributeOverride(name = "type", column = @Column(name = "modifier_type"))
  })
  @Valid
  private UserId modifierId;

  @DiffIgnore
  @ToString.Exclude
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumns({
    @JoinColumn(
        name = "modifier_id",
        columnDefinition = "uuid",
        insertable = false,
        updatable = false,
        referencedColumnName = "id"),
    @JoinColumn(
        name = "modifier_type",
        insertable = false,
        updatable = false,
        referencedColumnName = "type")
  })
  @Valid
  private UserSnap modifier;

  @PrePersist
  private void onCreate() {
    created = OffsetDateTime.now();
  }

  @PreUpdate
  private void onUpdate() {
    modified = OffsetDateTime.now();
  }

  public void setAuthor(@NotNull @Valid UserSnap author) {
    this.author = author;
    this.authorId = author.getCid();
  }

  public void setModifier(@NotNull @Valid UserSnap modifier) {
    this.modifier = modifier;
    this.modifierId = modifier.getCid();
  }

  @SuppressWarnings("AbstractClassNeverImplemented")
  public abstract static class VersionedEntityBuilder<
      C extends VersionedEntity, B extends VersionedEntityBuilder<C, B>> {
    public B copyVersionedDataFrom(@NotNull @Valid VersionedEntity entity) {
      id(entity.id);
      created(entity.created);
      modified(entity.modified);
      version(entity.version);
      author(entity.author);
      authorId(entity.authorId);
      modifierId(entity.modifierId);
      modifier(entity.modifier);
      return self();
    }
  }
}
