package pl.com.rszewczyk.common.http.problem;

import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;

@RequiredArgsConstructor
public class SpringDomainEventPublisher {
  private final ApplicationEventPublisher publisher;

  public void publish(Object event) {
    publisher.publishEvent(event);
  }
}
