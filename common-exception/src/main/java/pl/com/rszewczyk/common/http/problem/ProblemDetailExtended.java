package pl.com.rszewczyk.common.http.problem;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.http.ProblemDetail;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@SuppressFBWarnings({"EQ_OVERRIDING_EQUALS_NOT_SYMMETRIC", "USBR_UNNECESSARY_STORE_BEFORE_RETURN"})
public class ProblemDetailExtended extends ProblemDetail {
  private String instanceWithType;
  private ProblemLogLevel problemLogLevel;
  private String error;

  @Override
  public String toString() {
    return "["
        + "title='"
        + this.getTitle()
        + "', status='"
        + this.getStatus()
        + "', detail="
        + this.getDetail()
        + "', instance='"
        + this.instanceWithType
        + ", problemLogLevel='"
        + this.problemLogLevel
        + ", error='"
        + this.error
        + "']";
  }
}
