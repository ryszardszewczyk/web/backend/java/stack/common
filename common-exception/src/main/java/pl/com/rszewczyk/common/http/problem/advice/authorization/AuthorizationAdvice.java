package pl.com.rszewczyk.common.http.problem.advice.authorization;

import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.NativeWebRequest;
import pl.com.rszewczyk.common.http.problem.advice.Advice;
import pl.com.rszewczyk.common.http.problem.exception.UnauthorizedException;

@Order(100)
@RestControllerAdvice
public class AuthorizationAdvice extends Advice {

  @ExceptionHandler
  public ResponseEntity<ProblemDetail> handle(AccessDeniedException ex, NativeWebRequest request) {
    return create(HttpStatus.FORBIDDEN, ex, request, ex.getMessage());
  }

  @ExceptionHandler
  public ResponseEntity<ProblemDetail> handle(UnauthorizedException ex, NativeWebRequest request) {
    return create(HttpStatus.UNAUTHORIZED, ex, request, ex.getMessage());
  }

  @ExceptionHandler
  public ResponseEntity<ProblemDetail> handle(
      BadCredentialsException ex, NativeWebRequest request) {
    return create(HttpStatus.UNAUTHORIZED, ex, request, ex.getMessage());
  }
}
