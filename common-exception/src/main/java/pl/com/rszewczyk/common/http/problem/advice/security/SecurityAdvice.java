package pl.com.rszewczyk.common.http.problem.advice.security;

import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.NativeWebRequest;
import pl.com.rszewczyk.common.http.problem.advice.Advice;
import pl.com.rszewczyk.common.http.problem.exception.UnauthorizedException;

@RestControllerAdvice
public class SecurityAdvice extends Advice {

  @ExceptionHandler
  public ResponseEntity<ProblemDetail> handle(UnauthorizedException ex, NativeWebRequest request) {
    return create(HttpStatus.UNAUTHORIZED, ex, request, ex.getErrorCode());
  }
}
