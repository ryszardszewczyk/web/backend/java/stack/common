package pl.com.rszewczyk.common.http.problem;

import org.springframework.context.ApplicationEvent;

public class ProblemOccurredEvent extends ApplicationEvent {

  public ProblemOccurredEvent(ProblemDetailExtended source) {
    super(source);
  }

  public ProblemDetailExtended getProblemDetail() {
    return (ProblemDetailExtended) getSource();
  }
}
