package pl.com.rszewczyk.common.http.problem.exception;

import pl.com.rszewczyk.common.http.problem.ErrorCode;
import pl.com.rszewczyk.common.http.problem.ErrorCodeAware;

public class UnauthorizedException extends RuntimeException implements ErrorCodeAware {

  public UnauthorizedException() {
    super("Unauthorized");
  }

  @Override
  public String getErrorCode() {
    return ErrorCode.UNAUTHORIZED.name();
  }
}
