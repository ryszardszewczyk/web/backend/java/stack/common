package pl.com.rszewczyk.common.http.problem.advice.jwt;

import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.NativeWebRequest;
import pl.com.rszewczyk.common.http.problem.advice.Advice;

@RestControllerAdvice
public class JwtTokenAdvice extends Advice {

  @ExceptionHandler
  public ResponseEntity<ProblemDetail> handle(ExpiredJwtException ex, NativeWebRequest request) {
    return create(HttpStatus.UNAUTHORIZED, ex, request, null);
  }
}
