package pl.com.rszewczyk.common.http.problem;

public interface ErrorCodeAware {
  String getErrorCode();
}
