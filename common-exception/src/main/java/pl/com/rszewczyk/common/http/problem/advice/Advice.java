package pl.com.rszewczyk.common.http.problem.advice;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.util.Optional;
import javax.annotation.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ProblemDetail;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.NativeWebRequest;
import pl.com.rszewczyk.common.http.problem.ProblemDetailExtended;
import pl.com.rszewczyk.common.http.problem.ProblemLogLevel;
import pl.com.rszewczyk.common.http.problem.ProblemOccurredEvent;

public class Advice {
  @Autowired(required = false)
  private ApplicationEventPublisher eventPublisher;

  protected final ResponseEntity<ProblemDetail> create(
      @NotNull HttpStatusCode statusCode,
      Throwable exception,
      @NotNull NativeWebRequest request,
      @Nullable String error) {
    return create(toProblemDetail(statusCode, exception, request, error));
  }

  protected final ResponseEntity<ProblemDetail> create(@NotNull @Valid ProblemDetail problem) {
    publishEvent(problem);
    return ResponseEntity.status(problem.getStatus()).body(problem);
  }

  private static ProblemDetail toProblemDetail(
      HttpStatusCode status,
      Throwable exception,
      NativeWebRequest request,
      @Nullable String error) {
    ProblemDetailExtended problemDetailExtended = new ProblemDetailExtended();
    problemDetailExtended.setProblemLogLevel(ProblemLogLevel.WARN);
    problemDetailExtended.setStatus(status.value());
    problemDetailExtended.setDetail(exception.getMessage());
    problemDetailExtended.setInstanceWithType(getInstance(request));
    problemDetailExtended.setError(error);
    return problemDetailExtended;
  }

  protected static String getInstance(@NotNull NativeWebRequest request) {
    return Optional.ofNullable(request.getNativeRequest(HttpServletRequest.class))
        .map(rq -> String.format("%s [%s]", rq.getRequestURI(), rq.getMethod()))
        .orElse(null);
  }

  private void publishEvent(ProblemDetail problemDetail) {
    this.eventPublisher.publishEvent(
        new ProblemOccurredEvent((ProblemDetailExtended) problemDetail));
  }
}
