package pl.com.rszewczyk.common.http.problem;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

@SuppressFBWarnings("ENMI_ONE_ENUM_VALUE")
public enum ErrorCode {
  UNAUTHORIZED
}
