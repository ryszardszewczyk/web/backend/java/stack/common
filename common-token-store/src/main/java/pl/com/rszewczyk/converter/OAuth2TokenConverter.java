package pl.com.rszewczyk.converter;

import jakarta.validation.constraints.NotBlank;
import lombok.RequiredArgsConstructor;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.security.oauth2.core.OAuth2RefreshToken;
import org.springframework.stereotype.Component;
import pl.com.rszewczyk.util.JwtUtil;

@Component
@RequiredArgsConstructor
public class OAuth2TokenConverter {
  private final JwtUtil jwtUtil;

  public OAuth2AccessToken createOAuth2AccessToken(@NotBlank String token) {
    return new OAuth2AccessToken(
        tokenType(),
        token,
        jwtUtil.getJsonWebTokenIssuedAt(token),
        jwtUtil.getJsonWebTokenExpirationTime(token));
  }

  public OAuth2RefreshToken createOAuth2RefreshToken(@NotBlank String refreshToken) {
    return new OAuth2RefreshToken(
        refreshToken,
        jwtUtil.getJsonWebTokenIssuedAt(refreshToken),
        jwtUtil.getJsonWebTokenExpirationTime(refreshToken));
  }

  private static OAuth2AccessToken.TokenType tokenType() {
    return OAuth2AccessToken.TokenType.BEARER;
  }
}
