package pl.com.rszewczyk.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import pl.com.rszewczyk.converter.OAuth2TokenConverter;

@Configuration
@Import(OAuth2TokenConverter.class)
public class OAuth2TokenAutoConfiguration {}
