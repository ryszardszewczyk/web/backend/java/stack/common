package pl.com.rszewczyk.wrapper;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.time.Instant;
import java.util.Objects;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.security.oauth2.core.OAuth2RefreshToken;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@SuppressFBWarnings({
  "NP_NULL_ON_SOME_PATH_FROM_RETURN_VALUE",
  "USBR_UNNECESSARY_STORE_BEFORE_RETURN"
})
public class AccessToken {
  private OAuth2AccessToken oAuth2AccessToken;
  private OAuth2RefreshToken oAuth2RefreshToken;

  public long getExpiresInSeconds() {
    Instant expiresAt = oAuth2AccessToken.getExpiresAt();
    if (Objects.isNull(expiresAt)) {
      throw new IllegalStateException("Access token has no expiration date");
    }
    return expiresAt.minusSeconds(Instant.now().getEpochSecond()).getEpochSecond();
  }
}
