package pl.com.rszewczyk;

import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.core.OAuth2RefreshToken;
import pl.com.rszewczyk.wrapper.AccessToken;

public interface TokenStore {

  AccessToken getAccessToken(String email);

  void storeAccessToken(AccessToken accessToken, Authentication authentication);

  void removeAccessToken(AccessToken accessToken);

  void removeRefreshToken(OAuth2RefreshToken refreshToken);

  String generateAccessTokenKey(String name);

  String generateRefreshTokenKey(String name);

  boolean isAccessTokenValid(String accessToken);
}
