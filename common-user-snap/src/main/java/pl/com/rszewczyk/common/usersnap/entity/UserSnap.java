package pl.com.rszewczyk.common.usersnap.entity;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@Entity
@SuperBuilder
@EqualsAndHashCode(of = "cid")
@NoArgsConstructor
@AllArgsConstructor
@SuppressFBWarnings({"EI_EXPOSE_REP", "USBR_UNNECESSARY_STORE_BEFORE_RETURN"})
public class UserSnap {

  @Builder.Default @EmbeddedId @Valid private UserId cid = new UserId();

  @NotBlank private String name;

  private Boolean deleted;

  public UUID getId() {
    return cid.getId();
  }
}
