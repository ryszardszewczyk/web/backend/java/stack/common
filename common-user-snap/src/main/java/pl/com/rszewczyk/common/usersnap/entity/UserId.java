package pl.com.rszewczyk.common.usersnap.entity;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@Embeddable
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@SuppressFBWarnings("USBR_UNNECESSARY_STORE_BEFORE_RETURN")
public class UserId implements Serializable {

  @NotNull
  @Column(columnDefinition = "uuid", updatable = false)
  private UUID id;

  @NotBlank private String type;
}
