CREATE TABLE IF NOT EXISTS named_object_snap (
    id UUID NOT NULL,
    type TEXT NOT NULL,
    name TEXT NOT NULL,
    deleted BOOLEAN DEFAULT FALSE,
    PRIMARY KEY (id, type)
);

INSERT INTO
    named_object_snap
VALUES
    (
        '00000000-0000-0000-0000-000000000000',
        'USER',
        'system'
    )
ON CONFLICT DO NOTHING;

INSERT INTO
    named_object_snap
VALUES
    (
        '00000000-0000-0000-0000-000000000001',
        'USER',
        'anonymous'
    )
ON CONFLICT DO NOTHING;
