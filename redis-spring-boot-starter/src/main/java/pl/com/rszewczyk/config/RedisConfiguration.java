package pl.com.rszewczyk.config;

import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import pl.com.rszewczyk.TokenStore;
import pl.com.rszewczyk.converter.OAuth2TokenConverter;
import pl.com.rszewczyk.store.RedisTokenStore;
import pl.com.rszewczyk.util.JwtUtil;

@Configuration
public class RedisConfiguration {

  @Bean
  @ConditionalOnMissingBean(RedisTemplate.class)
  public RedisTemplate<String, Object> redisTemplate(
      RedisConnectionFactory redisConnectionFactory,
      RedisSerializer<Object> genericJackson2JsonRedisSerializer) {
    RedisTemplate<String, Object> template = new RedisTemplate<>();
    template.setConnectionFactory(redisConnectionFactory);
    template.setKeySerializer(new StringRedisSerializer());
    template.setValueSerializer(genericJackson2JsonRedisSerializer);
    return template;
  }

  @Bean
  @ConditionalOnMissingBean(GenericJackson2JsonRedisSerializer.class)
  public GenericJackson2JsonRedisSerializer genericJackson2JsonRedisSerializer() {
    GenericJackson2JsonRedisSerializer serializer = new GenericJackson2JsonRedisSerializer();
    serializer.configure(objectMapper -> objectMapper.registerModule(new JavaTimeModule()));
    return serializer;
  }

  @Bean
  @ConditionalOnMissingBean(TokenStore.class)
  public TokenStore tokenStore(
      RedisTemplate<String, Object> redisTemplate,
      OAuth2TokenConverter tokenConverter,
      JwtUtil jwtUtil) {
    return new RedisTokenStore(tokenConverter, redisTemplate, jwtUtil);
  }
}
