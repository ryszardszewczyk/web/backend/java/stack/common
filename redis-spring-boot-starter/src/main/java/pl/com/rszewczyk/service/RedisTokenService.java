package pl.com.rszewczyk.service;

import java.util.Objects;
import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import pl.com.rszewczyk.TokenStore;
import pl.com.rszewczyk.common.http.problem.exception.UnauthorizedException;
import pl.com.rszewczyk.converter.OAuth2TokenConverter;
import pl.com.rszewczyk.util.JwtUtil;
import pl.com.rszewczyk.wrapper.AccessToken;

@Service
@RequiredArgsConstructor
public class RedisTokenService {
  private final JwtUtil jwtUtil;
  private final OAuth2TokenConverter oAuth2TokenConverter;
  private final RedisTemplate<String, Object> redisTemplate;
  private final TokenStore tokenStore;

  public AccessToken generateAccessToken(Authentication authentication) {
    AccessToken existingAccessToken = this.tokenStore.getAccessToken(authentication.getName());

    if (Objects.nonNull(existingAccessToken)) {
      this.tokenStore.storeAccessToken(existingAccessToken, authentication);
      return existingAccessToken;
    }

    String accessToken = jwtUtil.generateAccessToken(authentication);
    String refreshToken = jwtUtil.generateRefreshToken(authentication);

    String accessTokenKey = tokenStore.generateAccessTokenKey(authentication.getName());
    String refreshTokenKey = tokenStore.generateRefreshTokenKey(authentication.getName());

    saveTokens(accessTokenKey, accessToken, refreshTokenKey, refreshToken);

    return AccessToken.builder()
        .oAuth2AccessToken(oAuth2TokenConverter.createOAuth2AccessToken(accessToken))
        .oAuth2RefreshToken(oAuth2TokenConverter.createOAuth2RefreshToken(refreshToken))
        .build();
  }

  public AccessToken refreshAccessToken(String existingRefreshToken) {
    AccessToken existingAccessToken =
        this.tokenStore.getAccessToken(jwtUtil.getJsonWebTokenSubject(existingRefreshToken));

    if (Objects.isNull(existingAccessToken)) {
      throw new UnauthorizedException();
    }

    String accessToken = jwtUtil.generateAccessToken(jwtUtil.getClaims(existingRefreshToken));
    String refreshToken = jwtUtil.generateRefreshToken(jwtUtil.getClaims(existingRefreshToken));

    String accessTokenKey = tokenStore.generateAccessTokenKey(accessToken);
    String refreshTokenKey = tokenStore.generateRefreshTokenKey(refreshToken);

    saveTokens(accessTokenKey, accessToken, refreshTokenKey, refreshToken);

    return AccessToken.builder()
        .oAuth2AccessToken(oAuth2TokenConverter.createOAuth2AccessToken(accessToken))
        .oAuth2RefreshToken(oAuth2TokenConverter.createOAuth2RefreshToken(refreshToken))
        .build();
  }

  public void removeAccessToken(String name) {
    AccessToken accessToken = this.tokenStore.getAccessToken(name);

    if (Objects.nonNull(accessToken)) {
      this.tokenStore.removeAccessToken(accessToken);
      this.tokenStore.removeRefreshToken(accessToken.getOAuth2RefreshToken());
    }
  }

  public boolean hasAccessToken(String accessToken) {
    return this.tokenStore.isAccessTokenValid(accessToken);
  }

  private void saveTokens(
      String accessTokenKey, String accessToken, String refreshTokenKey, String refreshToken) {
    redisTemplate.opsForValue().set(accessTokenKey, accessToken);
    redisTemplate.opsForValue().set(refreshTokenKey, refreshToken);
  }
}
