package pl.com.rszewczyk.store;

import io.jsonwebtoken.ExpiredJwtException;
import java.util.Objects;
import javax.annotation.Nullable;
import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.security.oauth2.core.OAuth2RefreshToken;
import pl.com.rszewczyk.TokenStore;
import pl.com.rszewczyk.converter.OAuth2TokenConverter;
import pl.com.rszewczyk.util.JwtUtil;
import pl.com.rszewczyk.wrapper.AccessToken;

@RequiredArgsConstructor
public class RedisTokenStore implements TokenStore {
  private final OAuth2TokenConverter oAuth2TokenConverter;
  private final RedisTemplate<String, Object> redisTemplate;
  private final JwtUtil jwtUtil;

  @Override
  @Nullable
  public AccessToken getAccessToken(String email) {
    String accessToken = (String) redisTemplate.opsForValue().get(generateAccessTokenKey(email));
    String refreshToken = (String) redisTemplate.opsForValue().get(generateRefreshTokenKey(email));
    if (Objects.isNull(accessToken)) {
      return null;
    }
    OAuth2AccessToken oAuth2AccessToken;
    OAuth2RefreshToken oAuth2RefreshToken;
    try {
      oAuth2AccessToken = oAuth2TokenConverter.createOAuth2AccessToken(accessToken);
      oAuth2RefreshToken = oAuth2TokenConverter.createOAuth2RefreshToken(refreshToken);
    } catch (ExpiredJwtException e) {
      return null;
    }

    return AccessToken.builder()
        .oAuth2RefreshToken(oAuth2RefreshToken)
        .oAuth2AccessToken(oAuth2AccessToken)
        .build();
  }

  @Override
  public void storeAccessToken(AccessToken accessToken, Authentication authentication) {
    redisTemplate
        .opsForValue()
        .set(
            generateAccessTokenKey(authentication.getName()),
            accessToken.getOAuth2AccessToken().getTokenValue());
    redisTemplate
        .opsForValue()
        .set(
            generateRefreshTokenKey(authentication.getName()),
            accessToken.getOAuth2RefreshToken().getTokenValue());
  }

  @Override
  public void removeAccessToken(AccessToken accessToken) {
    redisTemplate.delete(
        generateAccessTokenKey(
            jwtUtil.getJsonWebTokenSubject(accessToken.getOAuth2AccessToken().getTokenValue())));
  }

  @Override
  public void removeRefreshToken(OAuth2RefreshToken refreshToken) {
    redisTemplate.delete(
        generateRefreshTokenKey(jwtUtil.getJsonWebTokenSubject(refreshToken.getTokenValue())));
  }

  @Override
  public String generateAccessTokenKey(String name) {
    return name + "-access-token";
  }

  @Override
  public String generateRefreshTokenKey(String name) {
    return name + "-refresh-token";
  }

  @Override
  public boolean isAccessTokenValid(String accessToken) {
    return containsAccessToken(accessToken) && jwtUtil.isValid(accessToken);
  }

  private boolean containsAccessToken(String accessToken) {
    return Objects.nonNull(
        redisTemplate
            .opsForValue()
            .get(generateAccessTokenKey(jwtUtil.getJsonWebTokenSubject(accessToken))));
  }
}
