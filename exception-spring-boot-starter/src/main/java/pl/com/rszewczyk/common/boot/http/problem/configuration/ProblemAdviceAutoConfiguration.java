package pl.com.rszewczyk.common.boot.http.problem.configuration;

import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import pl.com.rszewczyk.common.http.problem.advice.authorization.AuthorizationAdvice;
import pl.com.rszewczyk.common.http.problem.advice.jwt.JwtTokenAdvice;
import pl.com.rszewczyk.common.http.problem.advice.security.SecurityAdvice;

@AutoConfigureBefore(WebMvcAutoConfiguration.class)
@Configuration
@Import({JwtTokenAdvice.class, AuthorizationAdvice.class, SecurityAdvice.class})
public class ProblemAdviceAutoConfiguration {}
