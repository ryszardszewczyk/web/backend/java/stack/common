package pl.com.rszewczyk.common.boot.http.problem;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.ObjectUtils;
import org.springframework.context.event.EventListener;
import pl.com.rszewczyk.common.http.problem.ProblemLogLevel;
import pl.com.rszewczyk.common.http.problem.ProblemOccurredEvent;

@Slf4j
public class ProblemDetailLoggingListener {

  @EventListener
  public void handle(ProblemOccurredEvent problemOccurredEvent) {
    ProblemLogLevel problemLogLevel =
        (ProblemLogLevel)
            ObjectUtils.defaultIfNull(
                problemOccurredEvent.getProblemDetail().getProblemLogLevel(), ProblemLogLevel.WARN);
    String logMessage =
        String.format("Problem: {problem: %s}", problemOccurredEvent.getProblemDetail());
    problemLogLevel.logger(log).accept(logMessage);
  }
}
