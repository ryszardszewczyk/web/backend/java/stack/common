package pl.com.rszewczyk.common.boot.http.problem.configuration;

import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import pl.com.rszewczyk.common.boot.http.problem.ProblemDetailLoggingListener;

@AutoConfigureBefore(WebMvcAutoConfiguration.class)
@Configuration
@Import(ProblemDetailLoggingListener.class)
public class ProblemLoggingAutoConfiguration {}
