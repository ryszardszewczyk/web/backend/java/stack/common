package pl.com.rszewczyk.config;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

@Data
@Validated
@Configuration
@NoArgsConstructor
@AllArgsConstructor
@ConfigurationProperties(prefix = "jwt-token-expiration")
@SuppressFBWarnings("USBR_UNNECESSARY_STORE_BEFORE_RETURN")
public class JwtTokenExpirationConfig {
  private Long accessTokenExpirationTime;
  private Long refreshTokenExpirationTime;
}
