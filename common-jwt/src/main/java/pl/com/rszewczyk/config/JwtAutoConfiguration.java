package pl.com.rszewczyk.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(JwtConfig.class)
public class JwtAutoConfiguration {}
