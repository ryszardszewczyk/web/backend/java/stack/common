package pl.com.rszewczyk.config;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

@Data
@Builder
@Validated
@Configuration
@NoArgsConstructor
@AllArgsConstructor
@ConfigurationProperties(prefix = "rsa")
@SuppressFBWarnings("USBR_UNNECESSARY_STORE_BEFORE_RETURN")
public class RSAKeyConfigProperties {
  private RSAPublicKey rsaPublicKey;
  private RSAPrivateKey rsaPrivateKey;
}
