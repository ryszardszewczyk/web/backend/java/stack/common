package pl.com.rszewczyk.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.SignatureException;
import jakarta.validation.constraints.NotBlank;
import java.time.Instant;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.jwt.JwtClaimsSet;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import org.springframework.security.oauth2.jwt.JwtEncoderParameters;
import org.springframework.stereotype.Component;
import pl.com.rszewczyk.config.JwtTokenExpirationConfig;
import pl.com.rszewczyk.config.RSAKeyConfigProperties;

@Component
@RequiredArgsConstructor
public class JwtUtil {
  private static final String JSON_WEB_TOKEN_ID = "jti";

  private final JwtEncoder encoder;
  private final JwtTokenExpirationConfig tokenExpirationConfig;
  private final RSAKeyConfigProperties rsaKeyConfigProperties;

  public String generateAccessToken(Authentication authentication) {
    Instant now = Instant.now();

    String scope =
        authentication.getAuthorities().stream()
            .map(GrantedAuthority::getAuthority)
            .collect(Collectors.joining(" "));

    JwtClaimsSet claims =
        JwtClaimsSet.builder()
            .issuedAt(now)
            .expiresAt(now.plusSeconds(tokenExpirationConfig.getAccessTokenExpirationTime()))
            .subject(authentication.getName())
            .claim("scope", scope)
            .claim(JSON_WEB_TOKEN_ID, authentication.getName())
            .build();

    return encoder.encode(JwtEncoderParameters.from(claims)).getTokenValue();
  }

  public String generateAccessToken(Claims claims) {
    Instant now = Instant.now();

    JwtClaimsSet claimsSet =
        JwtClaimsSet.builder()
            .issuedAt(now)
            .expiresAt(now.plusSeconds(tokenExpirationConfig.getAccessTokenExpirationTime()))
            .subject(claims.getSubject())
            .claim(JSON_WEB_TOKEN_ID, claims.getSubject())
            .build();
    return encoder.encode(JwtEncoderParameters.from(claimsSet)).getTokenValue();
  }

  public String generateRefreshToken(Authentication authentication) {
    Instant now = Instant.now();

    String scope =
        authentication.getAuthorities().stream()
            .map(GrantedAuthority::getAuthority)
            .collect(Collectors.joining(" "));

    JwtClaimsSet claims =
        JwtClaimsSet.builder()
            .issuedAt(now)
            .expiresAt(now.plusSeconds(tokenExpirationConfig.getRefreshTokenExpirationTime()))
            .subject(authentication.getName())
            .claim("scope", scope)
            .build();

    return encoder.encode(JwtEncoderParameters.from(claims)).getTokenValue();
  }

  public String generateRefreshToken(Claims claims) {
    Instant now = Instant.now();

    JwtClaimsSet claimsSet =
        JwtClaimsSet.builder()
            .issuedAt(now)
            .expiresAt(now.plusSeconds(tokenExpirationConfig.getRefreshTokenExpirationTime()))
            .subject(claims.getSubject())
            .build();

    return encoder.encode(JwtEncoderParameters.from(claimsSet)).getTokenValue();
  }

  public Instant getJsonWebTokenIssuedAt(@NotBlank String token) {
    return getClaims(token).getIssuedAt().toInstant();
  }

  public Instant getJsonWebTokenExpirationTime(@NotBlank String token) {
    return getClaims(token).getExpiration().toInstant();
  }

  public String getJsonWebTokenSubject(@NotBlank String token) {
    return getClaims(token).getSubject();
  }

  public boolean isValid(@NotBlank String token) {
    return getClaims(token).getExpiration().toInstant().isAfter(Instant.now());
  }

  public Claims getClaims(@NotBlank String token) {
    try {
      return Jwts.parser()
          .verifyWith(rsaKeyConfigProperties.getRsaPublicKey())
          .build()
          .parseSignedClaims(token)
          .getPayload();
    } catch (SignatureException e) {
      throw new IllegalStateException("Invalid JWT signature", e);
    }
  }
}
