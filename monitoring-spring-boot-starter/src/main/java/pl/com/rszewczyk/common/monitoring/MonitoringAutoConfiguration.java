package pl.com.rszewczyk.common.monitoring;

import com.zaxxer.hikari.HikariDataSource;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import io.micrometer.core.instrument.MeterRegistry;
import java.util.Objects;
import javax.annotation.Nullable;
import javax.sql.DataSource;
import javax.validation.constraints.NotNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
@ComponentScan
@SuppressFBWarnings("JVR_JDBC_VENDOR_RELIANCE")
public class MonitoringAutoConfiguration {

  @Configuration
  @ConditionalOnBean(DataSource.class)
  public static class HikariMetricsAutoConfiguration {

    public HikariMetricsAutoConfiguration(
        @NotNull DataSource dataSource, @Nullable MeterRegistry meterRegistry) {
      log.info("Registering HikariDataSource micrometer metrics");
      if (!(dataSource instanceof HikariDataSource) || Objects.isNull(meterRegistry)) {
        log.warn("HikariDataSource micrometer metrics has not been set");
        return;
      }
      ((HikariDataSource) dataSource).setMetricRegistry(meterRegistry);
      log.info("HikariDataSource micrometer metrics has been set");
    }
  }
}
