package pl.com.rszewczyk.security;

import com.google.common.base.MoreObjects;
import com.google.common.base.Splitter;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import javax.annotation.Nullable;
import lombok.Builder;
import lombok.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

@Value
@Builder
@SuppressFBWarnings("USBR_UNNECESSARY_STORE_BEFORE_RETURN")
@SuppressWarnings("UnstableApiUsage")
public class AccountIdentity implements Identity<UUID>, Serializable {

  private static final Integer ID_WITH_ROLE_PARTS = 2;
  private static final String SEPARATOR = "|";
  private static final String DEFAULT_ROLE = "CLIENT";
  private static final GrantedAuthority DEFAULT_AUTHORITY =
      new SimpleGrantedAuthority(DEFAULT_ROLE);

  private final UUID id;
  private final GrantedAuthority authority;

  @Nullable
  public static AccountIdentity fromHeaderString(@Nullable String idWithRole) {
    if (Objects.isNull(idWithRole)) {
      return null;
    }

    List<String> parts = Splitter.on(SEPARATOR).omitEmptyStrings().limit(2).splitToList(idWithRole);

    if (parts.isEmpty()) {
      return null;
    }

    String id = parts.getFirst();
    String role = DEFAULT_ROLE;

    if (parts.size() == ID_WITH_ROLE_PARTS) {
      role = parts.getLast();
    }

    return new AccountIdentity(UUID.fromString(id), new SimpleGrantedAuthority(role));
  }

  public GrantedAuthority getAuthority() {
    return MoreObjects.firstNonNull(authority, DEFAULT_AUTHORITY);
  }

  @Override
  public String getRole() {
    return getAuthority().getAuthority();
  }

  public String toHeaderString() {
    return id + SEPARATOR + getAuthority().getAuthority();
  }
}
