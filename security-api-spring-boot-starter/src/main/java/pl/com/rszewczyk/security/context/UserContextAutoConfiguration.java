package pl.com.rszewczyk.security.context;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(UserContextWebConfiguration.class)
public class UserContextAutoConfiguration {}
