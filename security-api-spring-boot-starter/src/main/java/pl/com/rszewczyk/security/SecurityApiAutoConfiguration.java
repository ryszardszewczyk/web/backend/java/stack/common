package pl.com.rszewczyk.security;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@Import({AccountIdentityService.class, UserContext.class})
public class SecurityApiAutoConfiguration {

  @Configuration
  public static class ArgumentResolverWebMvcConfigurer implements WebMvcConfigurer {
    @Autowired private AccountIdentityService accountIdentityService;

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
      resolvers.add(new AccountIdentityMethodArgumentResolver(accountIdentityService));
    }
  }

  @Configuration
  @EnableMethodSecurity
  @ConditionalOnProperty(value = "rszewczyk.security.enabled", havingValue = "true")
  public static class GlobalMethodSecurityConfig {}
}
