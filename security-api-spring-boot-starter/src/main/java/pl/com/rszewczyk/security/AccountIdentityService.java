package pl.com.rszewczyk.security;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.constraints.NotNull;
import java.util.Objects;
import javax.annotation.Nullable;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.NativeWebRequest;

@Service
public class AccountIdentityService {
  public static final String HEADER_IDENTITY_NAME = "x-account-id";

  @Nullable
  public AccountIdentity resolve(@NotNull NativeWebRequest request) {
    String[] headerValues = request.getHeaderValues(HEADER_IDENTITY_NAME);
    if (Objects.nonNull(headerValues) && headerValues.length > 0) {
      return resolveInternal(headerValues[0]);
    }
    return null;
  }

  @Nullable
  public AccountIdentity resolve(@NotNull HttpServletRequest request) {
    String headerValue = request.getHeader(HEADER_IDENTITY_NAME);
    if (!StringUtils.isEmpty(headerValue)) {
      return resolveInternal(headerValue);
    }
    return null;
  }

  @Nullable
  public static AccountIdentity resolveOnly(@NotNull HttpServletRequest request) {
    String headerValue = request.getHeader(HEADER_IDENTITY_NAME);
    if (!StringUtils.isEmpty(headerValue)) {
      return resolveInternal(headerValue, false);
    }
    return null;
  }

  private static AccountIdentity resolveInternal(@NotNull String headerValue) {
    return resolveInternal(headerValue, true);
  }

  private static AccountIdentity resolveInternal(
      @NotNull String headerValue, boolean setupContext) {
    AccountIdentity accountIdentity = AccountIdentity.fromHeaderString(headerValue);
    if (setupContext) {
      UserContext.setIdentity(accountIdentity);
    }
    return accountIdentity;
  }
}
