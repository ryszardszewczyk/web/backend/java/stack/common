package pl.com.rszewczyk.security;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public interface Identity<I> {

  @NotNull
  I getId();

  @NotBlank
  String getRole();
}
