package pl.com.rszewczyk.security.context;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class UserContextWebConfiguration implements WebMvcConfigurer {

  @Override
  public void addInterceptors(InterceptorRegistry registry) {
    registry.addInterceptor(new SetUserContextInterceptor()).order(1);
    registry.addInterceptor(new WithUserContextAnnotationInterceptor()).order(2);
  }
}
