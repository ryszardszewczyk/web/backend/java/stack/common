package pl.com.rszewczyk.security;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import jakarta.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import javax.annotation.Nullable;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

@ToString
@EqualsAndHashCode
@SuppressFBWarnings("USBR_UNNECESSARY_STORE_BEFORE_RETURN")
public class AccountAuthentication implements Authentication {
  private final AccountIdentity accountIdentity;

  public AccountAuthentication(@NotNull AccountIdentity accountIdentity) {
    this.accountIdentity =
        Objects.requireNonNull(accountIdentity, "Account identity cannot be null");
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return Collections.singleton(accountIdentity.getAuthority());
  }

  @Override
  public Object getCredentials() {
    return "";
  }

  @Nullable
  @Override
  public Object getDetails() {
    return null;
  }

  @Override
  public Object getPrincipal() {
    return accountIdentity;
  }

  @Override
  public boolean isAuthenticated() {
    return true;
  }

  @Override
  public void setAuthenticated(boolean isAuthenticated) {
    // empty
  }

  @Override
  public String getName() {
    return accountIdentity.getId().toString();
  }
}
