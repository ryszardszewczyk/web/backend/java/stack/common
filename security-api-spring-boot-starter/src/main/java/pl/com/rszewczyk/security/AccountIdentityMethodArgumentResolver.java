package pl.com.rszewczyk.security;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import jakarta.validation.constraints.NotNull;
import java.util.Objects;
import org.springframework.core.MethodParameter;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.annotation.AbstractNamedValueMethodArgumentResolver;

@SuppressFBWarnings("NP_NULL_ON_SOME_PATH_FROM_RETURN_VALUE")
@SuppressWarnings("CyclicClassDependency")
public class AccountIdentityMethodArgumentResolver
    extends AbstractNamedValueMethodArgumentResolver {
  public static final String HEADER_IDENTITY_NAME = AccountIdentityService.HEADER_IDENTITY_NAME;

  private final AccountIdentityService accountIdentityService;

  public AccountIdentityMethodArgumentResolver(
      @NotNull AccountIdentityService accountIdentityService) {
    super();
    this.accountIdentityService = accountIdentityService;
  }

  @Override
  protected NamedValueInfo createNamedValueInfo(MethodParameter parameter) {
    AccountPrincipal annotation = parameter.getParameterAnnotation(AccountPrincipal.class);
    return new RequestHeaderNamedValueInfo(Objects.requireNonNull(annotation));
  }

  @Override
  protected Object resolveName(String name, MethodParameter parameter, NativeWebRequest request) {
    return accountIdentityService.resolve(request);
  }

  @Override
  public boolean supportsParameter(MethodParameter parameter) {
    return parameter.hasParameterAnnotation(AccountPrincipal.class);
  }

  private static final class RequestHeaderNamedValueInfo extends NamedValueInfo {
    private RequestHeaderNamedValueInfo(@NotNull AccountPrincipal annotation) {
      super(HEADER_IDENTITY_NAME, annotation.required(), null);
    }
  }
}
