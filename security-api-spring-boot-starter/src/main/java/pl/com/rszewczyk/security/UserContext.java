package pl.com.rszewczyk.security;

import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import javax.annotation.Nullable;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@SuppressWarnings("PMD.AssignmentToNonFinalStatic")
public final class UserContext {
  public static final String FALLBACK_ID_PROPERTY_NAME = "rszewczyk.context.user.fallbackId";
  private static AccountIdentity fallbackUserId;

  private UserContext(@Value("${" + FALLBACK_ID_PROPERTY_NAME + ":#{null}}") UUID fallbackUserId) {
    if (Objects.isNull(fallbackUserId)) {
      log.warn("No fallback user id set ({}) -> ", FALLBACK_ID_PROPERTY_NAME);
    } else {
      UserContext.fallbackUserId = AccountIdentity.fromHeaderString(fallbackUserId.toString());
    }
  }

  public static void clean() {
    log.trace("Removing user identity: {}", SecurityContextHolder.getContext().getAuthentication());
    SecurityContextHolder.clearContext();
    MDC.remove("USER_ID");
  }

  public static void setIdentity(@Nullable AccountIdentity identity) {
    log.trace("Setting user identity: {}", identity);

    if (Objects.nonNull(identity)) {
      SecurityContextHolder.getContext().setAuthentication(new AccountAuthentication(identity));
      MDC.put("USER_ID", identity.toHeaderString());
    } else {
      clean();
      MDC.remove("USER_ID");
    }
  }

  @Nullable
  public static AccountIdentity getIdentity() {
    return getOptionalAccountIdentity().orElse(null);
  }

  public static AccountIdentity getIdentityOrThrow() {
    return Objects.requireNonNull(getIdentity(), "Account identity not set");
  }

  public static UUID getUserId() {
    return Optional.ofNullable(getAccountIdentity()).map(AccountIdentity::getId).orElse(null);
  }

  public static AccountIdentity getAccountIdentity() {
    return getOptionalAccountIdentity().orElse(fallbackUserId);
  }

  private static Optional<AccountIdentity> getOptionalAccountIdentity() {
    return Optional.ofNullable(SecurityContextHolder.getContext().getAuthentication())
        .map(Authentication::getPrincipal)
        .filter(AccountIdentity.class::isInstance)
        .map(AccountIdentity.class::cast);
  }
}
