package pl.com.rszewczyk.security.context;

import static pl.com.rszewczyk.security.AccountIdentityService.HEADER_IDENTITY_NAME;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.NotNull;
import java.util.Objects;
import java.util.Optional;
import javax.annotation.Nullable;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.messaging.handler.HandlerMethod;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.servlet.HandlerInterceptor;
import pl.com.rszewczyk.security.AccountIdentity;
import pl.com.rszewczyk.security.UserContext;

public class WithUserContextAnnotationInterceptor implements HandlerInterceptor {

  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
      throws Exception {
    if (!(handler instanceof HandlerMethod)) {
      return true;
    }

    WithUserContext annotation = getAnnotation((HandlerMethod) handler);
    AccountIdentity accountIdentity = UserContext.getAccountIdentity();

    if (Objects.nonNull(annotation) && annotation.required() && Objects.isNull(accountIdentity)) {
      throw new ServletRequestBindingException(
          "Missing request header '" + HEADER_IDENTITY_NAME + "'");
    }

    return true;
  }

  @Nullable
  private static WithUserContext getAnnotation(@NotNull HandlerMethod handlerMethod) {
    WithUserContext annotation = handlerMethod.getMethodAnnotation(WithUserContext.class);
    return Optional.ofNullable(annotation)
        .orElseGet(
            () ->
                AnnotationUtils.findAnnotation(handlerMethod.getBeanType(), WithUserContext.class));
  }
}
