package pl.com.rszewczyk.security.context;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.web.servlet.HandlerInterceptor;
import pl.com.rszewczyk.security.AccountIdentityService;
import pl.com.rszewczyk.security.UserContext;

@RequiredArgsConstructor
public class SetUserContextInterceptor implements HandlerInterceptor {

  @Override
  public boolean preHandle(
      HttpServletRequest request, HttpServletResponse response, Object handler) {
    UserContext.setIdentity(AccountIdentityService.resolveOnly(request));
    return true;
  }

  @Override
  public void afterCompletion(
      HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
    UserContext.clean();
  }
}
