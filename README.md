# common

Contains stuff to use across spring boot projects.

As of now it contains given modules:
1. [parent](parent)
2. [common-api](common-api)
